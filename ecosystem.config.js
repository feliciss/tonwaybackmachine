module.exports = {
  apps: [{
    name: `tonstorage-gateway-${process.env.NODE_ENV}`,
    script: './src/index.js',
    autorestart: true,
  }],
};
