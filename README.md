# TON Wayback Machine

The TON Wayback Machine is a tool similar to the [Internet Archive Wayback Machine](https://web.archive.org), but it focuses on archiving websites hosted on .ton domains.

## URL

The TON Wayback Machine can be accessed through the URL http://waybackmachine.ton, but it requires TON proxies to be accessed. To learn more about TON proxies and to download them, please visit the [GitHub repository](https://github.com/xssnick/Tonutils-Proxy).

## Fork of TON Storage Gateway

The TON Wayback Machine was created as a fork of the TON Storage Gateway. You can find more details about the TON Storage Gateway in the [GitHub repository](https://github.com/ton-blockchain/storage-gateway).

## Modifications

In the TON Wayback Machine, the `/gateway/` subroute has been removed from the original repository, leaving only hashes in subroutes.

## License

This project is released under the [AGPL-3.0-or-later](LICENSE) license.
